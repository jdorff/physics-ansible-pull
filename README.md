# Physics Ansible Pull
This is how most new Physics linux hosts are configured.

Notes:
 - Each hostname is it's own file and represents one system. The most common way to instal a host is to copy a file from a similar system.
 - The normal physics kickstart install post install add a custom rpm (http://install.phy.duke.edu/rpms/ans-0.4-1.noarch.rpm). This rpm contains the files to automate a periodic run of "ansible pull" against this repo.
 - For a laptop install, you need to ensure that the hostname is static, such that the same yaml file is referenced no matter what the local network is. 


Short comings:

 - Kind of messy with all the files in one directory.
 - The ansible 'includes' should be moved to "roles".
 - No confirmation from the host that has actually run the playbook sucessfully. You can mitigate this via checking the planisphere report tool for expected package configuration.
 - Not all Ansible playbook can be run here as there is no secret storage via ansible pull. We have a seperate repo we refer to as the "ansible push" repo that contains playbooks which require secrets. The two main "push" tasks are:
   - setting up a host with the planisphere report tool
   - setting up a host with sssd and authdir



